﻿namespace QuestSystem
{
  public enum QuestType
  {
    HUNT,
    GATHER,
    EXPLORE,
    TALK
  }
}