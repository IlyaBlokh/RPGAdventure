﻿namespace QuestSystem
{
  public enum QuestStatus
  {
    ACTIVE,
    FAILED,
    COMPLETED
  }
}