# RPGAdventure

3rd-person RPG on Unity

[![Gameplay](http://img.youtube.com/vi/osCme_ffLK8/0.jpg)](https://youtu.be/osCme_ffLK8 "Unity RPG Game")

*Click to watch video*

This is a classic 3-rd person RPG game with essential game mechanics:
- player control
- free-to-look camera
- combat system
- inventory
- quests
- dialogs with NPC
- experience and level up
- nice environment and SFX
